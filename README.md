# Yugipedia Redirect
This is a simple WebExtension for Google Chrome and Mozilla Firefox which
redirects all requests from [yugioh.wikia.com](http://yugioh.fandom.com) to [yugipedia.com](https://yugipedia.com).

## Download
* [Google Chrome](https://chrome.google.com/webstore/detail/yugipedia-redirect/ejenkocoekafkcabmnonnpploodghnkh)
* [Mozilla Firefox](https://addons.mozilla.org/en-GB/firefox/addon/yugipedia-redirect/)

If you want to install this extension manually then you can find the current ZIP [here](https://gitlab.com/severinkaderli/yugipedia-redirect/builds/artifacts/master/raw/yugipedia-redirect.zip?job=Build).