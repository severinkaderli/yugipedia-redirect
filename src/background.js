/**
 * The site where users will be redirected to.
 *
 * @type {String}
 */
const redirectHost = "https://yugipedia.com";

// Load extension state from settings store
let isActive = true;
chrome.storage.local.get(["isActive"], function(result) {
  console.log(result.isActive);
  if (result.isActive == undefined) {
    isActive = true;
  } else {
    isActive = result.isActive;
  }

  changeExtenionState(isActive);
});

// Handle redirection

chrome.webRequest.onBeforeRequest.addListener(
  function(details) {
    if (isActive) {
      return { redirectUrl: redirectHost + details.url.match(/^https?:\/\/[^\/]+([\S\s]*)/)[1] };
    }
  },
  {
    urls: [
      "*://yugioh.wikia.com/*",
      "*://www.yugioh.wikia.com/*",
      "*://yugioh.fandom.com/*",
      "*://www.yugioh.fandom.com/*"
    ],
    types: ["main_frame", "sub_frame", "stylesheet", "script", "image", "object", "xmlhttprequest", "other"]
  },
  ["blocking"]
);

// Enable or disable the extension using a browser action
chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.browserAction.getBadgeText({}, function(text) {
    changeExtenionState(text != "✔");
  });
});

/**
 * Changes the extension state to either enabled or disabled.
 *
 * @param {Boolean} state True if extension should be enabled, false otherwise.
 */
function changeExtenionState(state) {
  if (state) {
    chrome.browserAction.setBadgeText({ text: "✔" });
    chrome.browserAction.setTitle({ title: "Enabled" });
  } else {
    chrome.browserAction.setBadgeText({ text: "🗙" });
    chrome.browserAction.setTitle({ title: "Disabled" });
  }

  isActive = state;
  chrome.storage.local.set({ isActive: state });
}
