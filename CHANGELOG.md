# Changelog
## 1.2.0
* Add support for yugioh.fandom.com URLs

## 1.1.0
* Add on / off toggle

## 1.0.0
* Initial release